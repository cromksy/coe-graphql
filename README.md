# GraphQL Samples
> GraphQL 라이브러리 사용 샘플 프로젝트

GraphQL은 API를 구현하기 위한 REST 대신 사용되는 유연하고 강력한 대안입니다.

하나의 Endpoint가 모든 CRUD를 비롯한 행위를 처리합니다.

기존의 REST로 구현된 API는 URL과 리소스를 매칭시키는 개념적 모델을 사용했기 때문에 수 많은 엔드포인트를 사용하였지만 GraphQL은 모든 리소스가 마치 서로 연결 되어 있는 것처럼 보이기 때문에 URL을 별도로 분리 할 필요는 없습니다.

GraphQL은 URL이 아니라 쿼리를 통해서 표현하는 것이 핵심입니다.

## Getting started

1. Schema First Stryle

  - System Requirements

    | Type      	| Tool         	| Version      	|
    |-----------	|--------------	|--------------	|
    | Compiler  	| JDK         	| 1.8 이상     	|
    | Builder   	| maven        	| 3.2 이상     	|
    | Framework 	| Spring Boot  	| 2.0.4        	|
    |           	| graphql-java 	| 5.0.2 	|
    |           	| graphql-java-tools 	| 5.2.4 	|

2. Code First Style

  - System Requirements

    | Type      	| Tool         	| Version      	|
    |-----------	|--------------	|--------------	|
    | Compiler  	| JDK         	| 1.8 이상     	|
    | Builder   	| maven        	| 3.2 이상     	|
    | Framework 	| Spring Boot  	| 2.0.4        	|
    |           	| graphql-java 	| 5.0.2 	|
    |           	| graphql-java-tools 	| 5.2.4 	|
    |           	| spqr 	| 0.9.8 	|

## Related Links

- [MSA CoE Guide](https://coe.gitbook.io/guide/graphql/graphql)

- [Official site](https://graphql.org/learn/)
